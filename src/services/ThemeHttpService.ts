import { Tag } from "../models/Tag";
import { Theme } from "../models/Theme";
import { httpService } from "./httpService";

class ThemeHttpService {
  async findAll() {
    return httpService.get(`themes`);
  }

  async findById(themeId: number) {
    return httpService.get(`themes/${themeId}`);
  }

  async findArtByThemeId(themeId: number) {
    return httpService.get(`themes/${themeId}/arts`);
  }

  async findCurrent() {
    return httpService.get(`themes/current`);
  }

  async findNext() {
    return httpService.get(`themes/next`);
  }

  async save(theme: Theme) {
    return httpService.post(`themes`, theme);
  }

  async delete(themeId: number) {
    return httpService.delete(`themes/${themeId}`);
  }

  async findByIdAndTags(themeId: number, tags: Tag[]) {
    return httpService.post(`themes/${themeId}/arts/sort`, { tags: tags });
  }
}

export const themeService = Object.freeze(new ThemeHttpService());
