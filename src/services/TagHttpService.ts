import { httpService } from "./httpService";

class TagHttpService {
  async findAll() {
    return httpService.get(`tags`);
  }

  async findAllByTheme(themeId: number) {
    return httpService.get(`themes/${themeId}/arts/tags`);
  }
}

export const tagService = Object.freeze(new TagHttpService());
