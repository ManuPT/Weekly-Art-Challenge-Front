import { Art } from "../models/Art";
import { ArtPost } from "../models/ArtPost";
import { httpService } from "./httpService";

class ArtHttpService {
  async findAll() {
    return httpService.get(`arts`);
  }

  async findAllByCurrentTheme() {
    return httpService.get(`themes/current/arts`);
  }

  async findAllByLastTheme() {
    return httpService.get(`themes/last/arts`);
  }

  async findMostLikedToday() {
    return httpService.get(`/themes/current/arts/daily`);
  }

  async findCurrentArtByFollowed(idUser: number) {
    return httpService.get(`/themes/current/arts/followed/${idUser}`);
  }

  async findLastArtByFollowed(idUser: number) {
    return httpService.get(`/themes/last/arts/followed/${idUser}`);
  }

  async findTop10LastArtByCategory(idCategory: number) {
    return httpService.get(`/themes/last/arts/categories/${idCategory}/top10`);
  }

  async findById(artId: number) {
    return httpService.get(`arts/${artId}`);
  }

  async save(art: ArtPost) {
    return httpService.post(`arts`, art);
  }

  async delete(artId: number) {
    return httpService.delete(`arts/${artId}`);
  }
}

export const artService = Object.freeze(new ArtHttpService());
