
import { Follow } from "../models/Follow";
import { httpService } from "./httpService";

class FollowHttpService {

  async findAll() {
    return httpService.get(`follows`);
  }

  async findById(followId: number) {
    return httpService.get(`follows/${followId}`);
  }

  async save(follow: Follow) {
    return httpService.post(`follows`, follow);
  }

  async delete(followId: number) {
    return httpService.delete(`follows/${followId}`);
  }

  
  async followersList(artistId: number) {
    return httpService.get(`follows/followers/${artistId}`);
  }

  async isAlreadyFollowed(follow: Follow) {
    return httpService.post(`follows/exist`,follow);
  }

  async deleteByUserIdAndUserFollowedId(follow: Follow) {
    httpService.post(`follows/delete`,follow);
  }

}

export const followService = Object.freeze(new FollowHttpService());