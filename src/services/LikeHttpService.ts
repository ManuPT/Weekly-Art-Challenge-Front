import { Like } from "../models/Like";
import { httpService } from "./httpService";

class LikeHttpService {

  async findAll() {
    return httpService.get(`likes`);
  }

  async findById(likeId: number) {
    return httpService.get(`likes/${likeId}`);
  }

  async save(like: Like) {
    return httpService.post(`likes`, like);
  }

  async delete(likeId: number) {
    return httpService.delete(`likes/${likeId}`);
  }

  async isAlreadyLiked(like: Like) {
    return httpService.post(`likes/exists`,like);
  }

  async deleteByUserIdAndArtId(like: Like) {
    httpService.post(`likes/delete`,like);
  }

}

export const likeService = Object.freeze(new LikeHttpService());