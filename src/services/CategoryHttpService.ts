import { httpService } from "./httpService";

class CategoryHttpService {
  async findAll() {
    return httpService.get(`categories`);
  }
}

export const categoryService = Object.freeze(new CategoryHttpService());