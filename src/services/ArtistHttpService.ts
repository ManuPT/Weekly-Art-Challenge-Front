import { httpService } from "./httpService";

class ArtistHttpService {
  async findAll() {
    return httpService.get(`artists`);
  }

  async findById(artistId: number) {
    return httpService.get(`artists/${artistId}`);
  }

  async delete(artistId: number) {
    return httpService.delete(`artists/${artistId}`);
  }
}

export const artistService = Object.freeze(new ArtistHttpService());
