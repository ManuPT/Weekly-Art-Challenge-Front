import { UserLogin, UserRegistration} from "../models/User";
import { httpService } from "./httpService";

class LoginHttpService {
    async signUp(user : UserRegistration) {
        return httpService.post(`users`, user);
    }

    async signIn(userLogin : UserLogin) {
        return httpService.post(`users/login`, userLogin);
    }
}

export const loginService = Object.freeze(new LoginHttpService());