import React from 'react'
import { Route, Switch } from 'react-router-dom';
import { Landing } from './components/landing/Landing';
import { Profil } from './components/profil/Profil';
import { RegistrationComponent } from './components/registration/RegistrationComponent';
import { Submit } from './components/submit/Submit';
import { ListThemeComponent } from './components/theme/ListThemeComponent';
import { ThemeComponent } from './components/theme/ThemeComponent';


export const Routing = () => {
    return (
        <Switch>
            <Route path="/profil/:id">
                <Profil />
            </Route>
            <Route path="/themes/:id">
                <ThemeComponent />
            </Route>
            <Route path="/themes">
                <ListThemeComponent />
            </Route>
            <Route path="/arts/:id">
                {/* <ArtComponent /> */}
                <p>Hello World</p>
            </Route>
            <Route path="/registration">
                <RegistrationComponent />
            </Route>
            <Route path="/submit">
                <Submit />
            </Route>
            <Route path="/">
                <Landing />
            </Route>
        </Switch>
    )
}