import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { Art } from '../../models/Art';
import { Artist } from '../../models/Artist';
import { SocialMedia } from '../../models/SocialMedia';
import { artistService } from '../../services/ArtistHttpService';
import { ArtCardComponent } from '../art/ArtCardComponent';
import { TagComponent } from '../shared/TagComponent';
import { useHistory } from "react-router-dom";
import { followService } from '../../services/FollowHttpService';
import { RiFacebookBoxFill, RiInstagramFill, RiLinkedinBoxFill, RiPinterestFill, RiTwitterFill, RiUserAddLine, RiUserFollowLine, RiUserUnfollowLine, RiYoutubeFill } from 'react-icons/ri';
import { artService } from '../../services/ArtHttpService';

interface Params {
    id: string;
}

export const Profil = () => {
    const { id } = useParams<Params>();
    const [artist, setArtist] = useState<Artist>();
    const [idUser, setIdUser] = useState(Number);
    const [artSubmited, setArtSubmited] = useState<Art>();
    const history = useHistory();
    const [isAlreadyFollowed, setIsAlreadyFollowed ] = useState(Boolean);


    useEffect(() => {
        artistService.findById((+id)).then(setArtist);
        setIdUser(getUserFromLocalStorage);
    }, [id])

    useEffect(() => {
        setArtSubmited(artist?.soumission);
    }, [artist?.soumission])

    const update = () =>{
        artistService.findById((+id)).then(setArtist);
    }

    useEffect(() => {
        followService.isAlreadyFollowed(
            {
                "user": {
                    "id": idUser
                },
                "userFollowed": {
                    "id": +id
                }
            }
        ).then(setIsAlreadyFollowed);
    }, [id, idUser])

    const getUserFromLocalStorage = () => {
        let userString = localStorage.getItem("user");
        let json = JSON.parse(userString||'{"id":null}');
        return json.id;
    }

    const changeState = () => {
        setIsAlreadyFollowed(!isAlreadyFollowed)
    }

    const follow = () => {
        if(idUser === null) {
            history.push('/registration')
        }
        else {
            followService.save(
                {
                    "user": {
                        "id": idUser
                    },
                    "userFollowed": {
                        "id": +id
                    }
                } 
            ).then(changeState);
        }
    }

    const unfollow = () => {
        followService.deleteByUserIdAndUserFollowedId(
            {
                "user": {
                    "id": idUser
                },
                "userFollowed": {
                    "id": +id
                }
            } 
        ).then(() => changeState() );
    }

    const [isFollowButtonHover, setIsFollowButtonHover] = useState(false);
    const followText = isFollowButtonHover ? "Unfollow" : "Following";

    const toggleFollowText = () => {
        setIsFollowButtonHover(!isFollowButtonHover);
    }

    const deleteSumbission = () => {
        const soumissionId = artist?.soumission.id || -1;
        artService.delete(soumissionId).then(() => setArtSubmited(undefined));
    }


    return (
        <div className="profil-container">
            <div className="profil-infos-container">
                <div className="profil-photo-container">
                    <img src="https://static-cse.canva.com/blob/189288/article_canva_le_guide_pour_creer_de_superbes_photos_de_profil_9-1.jpg" alt="" />
                    <div className="follow-btn-container">
                        { !isAlreadyFollowed && idUser !== +id && <button className="btn btn-follow" onClick={follow}>+ Follow</button>}
                        { isAlreadyFollowed && idUser !== +id && <button onMouseEnter={toggleFollowText} onMouseLeave={toggleFollowText} className="btn btn-follow" onClick={unfollow}>{followText}</button>}
                    </div>
                </div>
                <div className="profil-infos">
                    <h2 className="title">{artist?.firstname} {artist?.lastname}</h2>
                    <div className="profil-stats">
                        <div className="profil-stats-item">{artist?.nbParticipation} participations</div>
                        <div className="profil-stats-item">{artist?.nbFollow} followers</div>
                    </div>
                    <p>{artist?.description}</p>
                    <div className="profil-social-media">
                        {artist?.socialMedias.map((socialMedia, index) =>
                            <SocialMediaComponent key={index} socialMedia={socialMedia} />
                        )}
                    </div>
                    <div className="tags-list">
                        <h3 className="title">Favorite Tags</h3>
                        {artist?.tags.map((tag, index) =>
                            <TagComponent tag={tag} activable={false} key={index} />
                        )}
                    </div>
                </div>
            </div>
            <div className="profil-arts-container">
                {(artSubmited && (idUser === +id) ) ? 
                <>
                    <h2>Current Submission</h2>
                    <div className="card-mozaic profil-submission">
                        <div>
                        <ArtCardComponent art={artSubmited} update={update} />
                        </div>
                        <div>
                            <button className="button-full danger" onClick={deleteSumbission}>Delete Art</button>
                        </div>
                    </div>
                </>
                : null
                }
                <h2>Feed</h2>
                <div className="card-mozaic">
                    {artist?.arts.map((art, index) =>
                        <ArtCardComponent key={index} art={art} update={update}/>
                    )}
                </div>
            </div>
        </div>
    )
}

interface SMProps {
    socialMedia: SocialMedia;
}

const SocialMediaComponent = (props: SMProps) => {

    const selectLogo = (logoId: number, className: string) => {
        switch(logoId) {
            case 1: // Linkedin
                return <RiLinkedinBoxFill className={className}/>
            case 2: // Facebook
                return <RiFacebookBoxFill className={className} />
            case 3: // Twitter
                return <RiTwitterFill className={className} />
            case 4: // Youtube
                return <RiYoutubeFill className={className} />
            case 5: // Pinterest
                return <RiPinterestFill className={className} />
            case 6: // Instagram
                return <RiInstagramFill className={className} />
            default:
                return "";
        }
    }

    return (
        <a href={props.socialMedia.link}>
            {selectLogo(props.socialMedia.logo.id, "social-media")}
        </a>
    )

}
