import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { UserContext } from '../../App';

export const Header = () => {

    const listenToScroll = () => {
        const winScroll =
            document.body.scrollTop || document.documentElement.scrollTop

        setScrollPosition(winScroll);
    }

    useEffect(() => {
        window.addEventListener('scroll', listenToScroll);
    }, [])


    const resetScrollPosition = () => {
        window.scrollTo(0, 0);
    }

    const [scrollPosition, setScrollPosition] = useState(0);

    const { user, setUser } = useContext(UserContext);

    const logout = () => {
        localStorage.removeItem("user");
        setUser({
            id: -1,
            pseudo: "",
            mail: "",
            role: {
                id: -1,
                name: ""
            },
            firstname: "",
            lastname: "",
            description: ""
        });
    }

    return (
        <header className={(scrollPosition !== 0 ? "scrolled" : "")}>
            <div className={`header-container`}>
                <div className="header-left">
                    <div className="logo">
                        <Link onClick={resetScrollPosition} to="/"><h1>WAC</h1></Link>
                    </div>
                    <div className="menu">
                        <Link onClick={resetScrollPosition} className="header-item" to="/themes">Themes</Link>
                    </div>
                </div>
                <div className="header-right">
                    <div className="menu">
                        {user.id !== -1 ?
                            <>
                                <Link onClick={resetScrollPosition} className="header-item" to={`/profil/${user.id}`}>
                                    Profil
                                </Link>
                                <span className="header-item" onClick={logout}>Log out</span>
                            </>
                            :
                            <>
                                <Link onClick={resetScrollPosition} className="header-item"
                                    to={{
                                        pathname: "/registration",
                                        state: { signOut: false }
                                    }}>
                                    Sign In
                                </Link>
                                <Link onClick={resetScrollPosition} className="header-item"
                                    to={{
                                        pathname: "/registration",
                                        state: { signOut: true }
                                    }}>
                                    Sign up
                                </Link>
                            </>
                        }
                    </div>
                </div>
            </div>
        </header>
    )
}