import React, { useContext, useEffect, useState } from 'react';
import { UserContext } from '../../App';
import { Art } from '../../models/Art';
import { Theme } from '../../models/Theme';
import { artService } from '../../services/ArtHttpService';
import { themeService } from '../../services/ThemeHttpService';
import { ListArtComponent } from '../art/ListArtComponent';
import { ButtonComponent } from '../shared/ButtonComponent';
import { CustomCountdownComponent } from '../shared/CustomCountdownComponent';

export const Landing = () => {
  const [currentTheme, setCurrentTheme] = useState<Theme>()
  const [nextTheme, setNextTheme] = useState<Theme>()
  const [currentArtsFollowed, setCurrentArtsFollowed] = useState<Art[]>([]);
  const [lastArtsFollowed, setLastArtsFollowed] = useState<Art[]>([]);
  const [currentArtsMostLikedToday, setCurrentArtsMostLikedToday] = useState<Art[]>([]);
  const [lastArtsTop10Music, setLastArtsTop10Music] = useState<Art[]>([]);
  const [lastArtsTop10Painting, setLastArtsTop10Painting] = useState<Art[]>([]);
  const [lastArtsTop10Carving, setLastArtsTop10Carving] = useState<Art[]>([]);

  const {user, setUser} = useContext(UserContext);

  useEffect(() => {
    update();
  }, []);

  useEffect(() => {
    if(user.id!==-1){
      artService.findCurrentArtByFollowed(user.id).then(setCurrentArtsFollowed);
      artService.findLastArtByFollowed(user.id).then(setLastArtsFollowed);
    }
  }, [user]);

  const update = () =>{
    themeService.findCurrent().then(res => capitalizeTheme(res, setCurrentTheme));
    themeService.findNext().then(res => capitalizeTheme(res, setNextTheme));
    artService.findMostLikedToday().then(setCurrentArtsMostLikedToday);
    artService.findTop10LastArtByCategory(1).then(setLastArtsTop10Music);
    artService.findTop10LastArtByCategory(3).then(setLastArtsTop10Painting);
    artService.findTop10LastArtByCategory(4).then(setLastArtsTop10Carving);
  }

  const capitalizeTheme = (res: Theme, fct: Function) => {
    const str = res.name;
    res.name = str.charAt(0).toUpperCase() + str.slice(1);
    fct(res);
  }

  return (
    <>
      <div className="landing-container">
        <div className="landing-card landing-info-theme">
          <h1 className="landing-info-theme-title">WE CHANGE THE WORLD</h1>
          <span className="landing-info-theme-content">
            Vote for this week's theme <span className="landing-theme-span">{ currentTheme?.name }</span>, you can also submit for the next one <span className="landing-theme-span">{ nextTheme?.name }</span>.
          </span>
          <div className="landing-cta">
            <ButtonComponent to={"submit"} text={"Submit your Work!"} />
          </div>
        </div>
        <div className="landing-card landing-bg-container">
          <img className="landing-bg-img" src="https://burst.shopifycdn.com/photos/woman-grasping-flowers.jpg?width=1850&format=pjpg&exif=1&iptc=1" alt="" />
        </div>
        <CustomCountdownComponent date={currentTheme?.dateEnd} />
      </div>

      <LandingSection arts={currentArtsMostLikedToday} title={"Current Theme : Most liked today"} update={update}/>
      {user.id!==-1&& currentArtsFollowed.length!==0 && <LandingSection arts={currentArtsFollowed} title={"Current Theme : Followed"} update={update} />}
      <LandingSection arts={lastArtsTop10Music} title={"Last Theme : Top 10 Music"} update={update}/>
      <LandingSection arts={lastArtsTop10Painting} title={"Last Theme : Top 10 Painting"} update={update}/>
      <LandingSection arts={lastArtsTop10Carving} title={"Last Theme : Top 10 Carving"} update={update}/>
      {user.id!==-1&& lastArtsFollowed.length!==0 && <LandingSection arts={lastArtsFollowed} title={"Last Theme : Followed"} update={update}/>}
      
    </>
  )
}

interface LandingSectionProps {
  arts: Art[]
  title: string;
  update: any;
}

const LandingSection = (props: LandingSectionProps) => {
  return (
    <div className="landing-carousel-main-container">
      <div className="landing-carousel-bg"></div>
      <div className="landing-carousel-container">
        <div className="landing-carousel-title">
          <h2>{props.title}</h2>
        </div>
        <ListArtComponent arts={props.arts} update={props.update}/>
        <div className="see-more">
          {props.arts.length>0 && <ButtonComponent to={"/themes/"+props.arts[0].theme.id} text={"See more"} /> }
        </div>
      </div>
    </div>
  )
}