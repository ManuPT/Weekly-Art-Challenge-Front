import React, { useState } from 'react';
import './css/Tag.css';
import { Tag } from '../../models/Tag';

interface Props {
    tag: Tag;
    activable?: boolean;
    filter?: any;
}

export const TagComponent = (props: Props) => {

    const [isActive, setIsActive] = useState(false);

    const isActivableClass = props.activable ? "activable" : "";
    const isActiveClass = isActive ? "active" : "";

    const toggleActiveTag = () => {
        if (!isActivableClass)
            return;
        if(props.filter!==undefined) props.filter(props.tag);
        setIsActive(!isActive);
    }

    return (
        <div onClick={toggleActiveTag} className={`tag ${isActivableClass} ${isActiveClass}`}>{props.tag.name}</div>
    )
}