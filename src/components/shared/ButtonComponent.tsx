import React from 'react';
import './css/Button.css';
import { Link } from 'react-router-dom';

interface Props {
    to: string;
    text: string;
}

export const ButtonComponent = (props: Props) => {

    return (
        <Link className="btn button-arrow" to={props.to}>
            <span className='arrow arrow-left'>
                <span className='shaft'></span>
            </span>
            <span className='main'>
                <span className='text'>
                    {props.text}
                </span>
                <span className='arrow arrow-right'>
                    <span className='shaft'></span>
                </span>
            </span>
        </Link>
    )
}