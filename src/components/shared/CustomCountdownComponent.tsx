import React from 'react';
import Countdown from 'react-countdown';

interface Props {
    date?: Date;
}

export const CustomCountdownComponent = (props: Props) => {

    const renderer = ((props: any) => {
    
        // Render a countdown
        return (
            <div className="countdown">
                <ul>
                    <li><span id="days">{props.formatted.days}</span>days</li>
                    <li><span id="hours">{props.formatted.hours}</span>Hours</li>
                    <li><span id="minutes">{props.formatted.minutes}</span>Minutes</li>
                    <li><span id="seconds">{props.formatted.seconds}</span>Seconds</li>
                </ul>
            </div>
        );
    
    });

    return (
    <div className="countdown-container">
        { props.date ?
                <Countdown date={props.date} renderer={renderer}
                    zeroPadTime={2}/>
        : null }
        <div className="countdown-text">Left before the next Theme, <br />don't forget to vote !</div>
    </div>
    )
}