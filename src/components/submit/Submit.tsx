import React, { useEffect, useState } from 'react';
import { Tag } from '../../models/Tag';
import { Category } from '../../models/Category';
import { categoryService } from '../../services/CategoryHttpService';
import { tagService } from '../../services/TagHttpService';
import { TagComponent } from '../shared/TagComponent';
import { artService } from '../../services/ArtHttpService';
import { ArtPost } from '../../models/ArtPost';
import { ArtContentPreviewComponent } from '../art/ArtContentPreviewComponent';
import { useHistory } from "react-router-dom";

export const Submit = () => {

    const [idUser, setIdUser] = useState(Number);
    const [content, setContent] = useState("");
    const [name, setName] = useState("");
    const [idCategory, setIdCategory] = useState(1);

    const [categories, setCategories] = useState<Category[]>([]);
    const [tags, setTags] = useState<Tag[]>([]);
    const [activeTags, setActiveTags] = useState<Tag[]>([]);
    const [previewArt, setPreviewArt] = useState<ArtPost>();
    const history = useHistory();


    useEffect(() => {
        categoryService.findAll().then(setCategories);
        tagService.findAll().then(setTags);
        setIdUser(getUserFromLocalStorage);
      }, []);

    const fillerArt: ArtPost = {
            "name": "filler",
            "content": "https://via.placeholder.com/720x450?text=Preview+of+your+Art",
            category: {
                id: 2
            },
            user: {
                id: idUser
            },
            tags: activeTags
    }

    const updateSort = (childrenTag:Tag) =>{
        let findTag:Tag|undefined;
        findTag = activeTags.find((tag:Tag) => tag.id===childrenTag.id);
        if(findTag===undefined) setActiveTags([...activeTags,childrenTag]);
        else{
            let newList = activeTags.filter((tag:Tag) => tag.id!==childrenTag.id);
            setActiveTags(newList);
        }
    }


    const getUserFromLocalStorage = () => {
        let userString = localStorage.getItem("user");
        if(userString===null){
            history.push('/registration')
        }
        let json = JSON.parse(userString||'{"id":1}');
        return json.id;
    }

    const sendArt = () => {
        let json = jsonMaker();
        artService.save(json).then(redirect);
    }

    const redirect = () =>{
        history.push(`/profil/${idUser}`);
    }

    const tryPreviewArt = () => {
        let json = jsonMaker();
        setPreviewArt(json);
    }

    const jsonMaker = () =>{
        return {
            name,
            content,
            category: {
                id: idCategory
            },
            user: {
                id: idUser
            },
            tags: activeTags
        }
    }

    return (
        <>
            <div className="registration-container submit-container" id="container">
                <div className="split">
                    <form action="#">
                        <h1>Submit Art !</h1>
                        <input type="text" placeholder="Name of the Art" name="name" value={name} onChange={(event)=>setName(event.target.value)}/>
                        <input type="text" placeholder="Url of your Art" name="content" value={content} onChange={(event)=>setContent(event.target.value)} />
                        <select className="custom-select" name="idCategory" id="idCategory" onChange={(event)=>setIdCategory(+event.target.value)}>
                            {categories.map((category:Category, index:number) => {
                                return <option key={index} value={category.id}>{category.name}</option>
                            })}
                        </select>
                        <div className="tags-container">
                            {tags.map((tag, index) =>
                                <TagComponent tag={tag} activable={true} key={index} filter={updateSort}/>
                            )}
                        </div>
                        <button className="button-full" onClick={(e)=>{e.preventDefault();sendArt()}}>Submit Art</button>
                    </form>
                </div>
                <div className="split">
                    {previewArt!==undefined && <ArtContentPreviewComponent art={previewArt}/>}
                    {previewArt===undefined && <ArtContentPreviewComponent art={fillerArt}/>}
                    <button className="button-full" onClick={(e)=>{e.preventDefault();tryPreviewArt()}}>Preview my Art</button>
                </div>
            </div>
        </>
    )
}