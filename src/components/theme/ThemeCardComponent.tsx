import React from 'react';
import Moment from 'moment';
import { Link } from 'react-router-dom';
import { Theme } from '../../models/Theme';

interface Props {
    theme: Theme;
}

export const ThemeCardComponent = (props: Props) => {
    Moment.locale('en');

    const dateFormat = "D MMM YYYY";

    const styleFunction = () =>{
        if(props.theme.periode.id===1) return "grid-item grid-disabled"
        else return "grid-item"
    }

    return (
        <Link className={styleFunction()} to={`/themes/${props.theme.id}`}>
            <h3 className="grid-item-title">{props.theme.name}</h3>
            <div className="grid-item-splitter"></div>
            <span className="grid-item-participation">
                {props.theme.periode.id === 1 ? "SOON" : `${props.theme.nbParticipation} Participations`}
            </span>
            <div className="grid-item-infos">
                <span>{Moment(props.theme.dateStart).format(dateFormat)}</span>
                <span>--</span>
                <span>{Moment(props.theme.dateEnd).format(dateFormat)}</span>
            </div>
        </Link>
    )
}