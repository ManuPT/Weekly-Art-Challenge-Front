import React, { useEffect, useState } from 'react';
import { Theme } from '../../models/Theme';
import { themeService } from '../../services/ThemeHttpService';
import { ThemeCardComponent } from './ThemeCardComponent';

export const ListThemeComponent = () => {

    const [themes, setThemes] = useState<Theme[]>([]);

    useEffect(() => {
        themeService.findAll().then(setThemes);
    }, []);

    return (
        <>
            <h2>List of Themes</h2>
            <div className="grid-container">
                {themes.map((theme, index) =>
                    <ThemeCardComponent key={index} theme={theme} />
                )}
            </div>
        </>
    )
}