import React, { useEffect, useState } from 'react';
import { RiArrowUpDownFill, RiFilter2Line } from 'react-icons/ri';
import { useParams } from 'react-router-dom';
import { Art } from '../../models/Art';
import { Tag } from '../../models/Tag';
import { Theme } from '../../models/Theme';
import { tagService } from '../../services/TagHttpService';
import { themeService } from '../../services/ThemeHttpService';
import { ArtCardComponent } from '../art/ArtCardComponent';
import { TagComponent } from '../shared/TagComponent';

interface Params {
    id: string; 
}

export const ThemeComponent = () => {

    const { id } = useParams<Params>();
    const [arts, setArts] = useState<Art[]>([]);
    const [theme, setTheme] = useState<Theme>();
    const [tags, setTags] = useState<Tag[]>([]);
    const [activeTags, setActiveTags] = useState<Tag[]>([]);

    useEffect(() => {
        themeService.findArtByThemeId((+id)).then(setArts);
        themeService.findById((+id)).then(setTheme);
        tagService.findAllByTheme((+id)).then(setTags);
    }, [id]);

    useEffect(() =>{
        if (activeTags.length > 0) 
            themeService.findByIdAndTags(+id,activeTags).then(setArts);
        else 
            themeService.findArtByThemeId((+id)).then(setArts);
    }, [activeTags, id])

    const update = () =>{
        themeService.findArtByThemeId((+id)).then(setArts);
    }

    const updateFilter = (childrenTag:Tag) =>{
        let findTag:Tag|undefined;
        findTag = activeTags.find((tag:Tag) => tag.id===childrenTag.id);
        
        if (findTag === undefined) 
            setActiveTags([...activeTags,childrenTag]);
        else {
            let newList = activeTags.filter((tag:Tag) => tag.id!==childrenTag.id);
            setActiveTags(newList);
        }
    }

    const updateSort = (value: String) => {
        let sorted: Art[] = [];

        switch(value) {
            case "MORE_LIKE":
                sorted = [...arts].sort(sortMoreLike);
                break;
            case "LESS_LIKE":
                sorted = [...arts].sort(sortMoreLike);
                sorted.reverse();
                break;
            case "AZ":
                sorted = [...arts].sort(sortAZ);
                break;
            case "ZA":
                sorted = [...arts].sort(sortAZ);
                sorted.reverse();
                break;
            default:
                return;
        }

        setArts(sorted);
    }


    const sortMoreLike = (a: Art, b: Art) => {
        if (a.nbrLikes > b.nbrLikes) {
            return -1;
        }
        if (a.nbrLikes < b.nbrLikes) {
            return 1;
        }
        return 0;
    }

    const sortAZ = (a: Art, b: Art) => {
        if (a.name < b.name) {
            return -1;
        }
        if (a.name > b.name) {
            return 1;
        }
        return 0;
    }

    return (<>
        <div className="theme-header">
            <h2>Theme: {theme?.name.toUpperCase()}</h2>
            <div className="art-filter-container">
                <div className="order-by">
                    <RiArrowUpDownFill />

                    <select onChange={(event) => updateSort(event.target.value)}>
                        <option defaultValue=""> -- Order by -- </option>
                        <option value="MORE_LIKE">Most Like</option>
                        <option value="LESS_LIKE">Less Like</option>
                        <option value="AZ">A - Z</option>
                        <option value="ZA">Z - A</option>
                    </select>

                    <div className="tags-list">
                        {/* <RiFilter2Line /> */}
                        {tags.map((tag, index) =>
                            <TagComponent tag={tag} activable={true} key={index} filter={updateFilter} />
                        )}
                    </div>
                </div>
                
            </div>
        </div>
        <div className="card-mozaic">
            {arts.map((art, index) =>
                <ArtCardComponent key={index} art={art} update={update} />
            )}
        </div>
        </>
    )
}