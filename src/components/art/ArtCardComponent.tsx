import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Art } from '../../models/Art';
import { TagComponent } from '../shared/TagComponent';
import { ArtContentComponent } from './ArtContentComponent';
import { RiHeartAddLine, RiHeartFill, RiHeartLine } from 'react-icons/ri';
import { useHistory } from "react-router-dom";
import { UserContext } from '../../App';
import { likeService } from '../../services/LikeHttpService';

interface Props {
    art: Art;
    update: any;
}

export const ArtCardComponent = (props: Props) => {

    const [isActive, setIsActive] = useState(false);
    const [isLiked, setIsLiked] = useState(false);
    const {user, setUser} = useContext(UserContext);
    const history = useHistory();
    const [originalNumberLike, setOriginalNumberLike] = useState(props.art.nbrLikes);


    const like = () => {
        if (user.id === -1){
            history.push('/registration')
        }
        else {
            likeService.save(
                {
                    "user": {"id": +user.id},
                    "art": {"id": props.art.id}
                } 
            ).then(changeState);
        }
        props.art.nbrLikes++;
    }

    const unlike = () => {
        likeService.deleteByUserIdAndArtId(
            {
                "user": {"id": +user.id},
                "art": {"id": props.art.id}
            } 
        ).then(changeState);
        props.art.nbrLikes--;
    }

    const changeState = () => {
        setIsLiked(!isLiked);
    }


    const classActive = isActive ? "show-details" : "";

    useEffect(() => {
        if(user.id!==-1){
          likeService.isAlreadyLiked(
                {
                    "user": {"id": +user.id},
                    "art": {"id": props.art.id}
                }
            ).then(setIsLiked);
        }
      }, [user]);

    const showDetails = () => {
        setIsActive(true);
    }

    const hideDetails = () => {
        setIsActive(false);
        if(originalNumberLike!==props.art.nbrLikes) props.update();
    }

    return (
        <>
            <div onClick={showDetails} className={`card art-card striped-shadow`}>
                <ArtContentComponent art={props.art} />
                {props.art.theme.periode.id !== 1 ?
                <div className="card-likes">
                    {props.art.nbrLikes} <RiHeartFill /> 
                </div>
                : null}
            </div>
            <div className={`art-details-container ${classActive}`}>
                <button onClick={hideDetails} className="close">X</button>
                <div className="art-details-content-container">
                    <div className="art-details-content">
                    <ArtContentComponent art={props.art} />
                    </div>
                </div>
                <div className="art-details">
                    <span className="art-details-theme">Theme: {props.art.theme.name}</span>
                    <h3>{props.art.name}</h3>
                    
                    <span className="art-details-author">—— {props.art.category.name} by <Link to={`/profil/${props.art.user.id}`}>{props.art.user.pseudo}</Link></span>
                    {!isLiked && props.art.theme.periode.id === 2 && <div className="art-details-like likable" onClick={like} style={{display: "flex", alignItems: "center"}}>{props.art.nbrLikes} <RiHeartAddLine /> </div>}
                    {isLiked && props.art.theme.periode.id === 2 && <div className="art-details-like likable" onClick={unlike} style={{ display: "flex", alignItems: "center" }}>{props.art.nbrLikes} <RiHeartFill /> </div> }
                    {!isLiked && props.art.theme.periode.id !== 2 && <div className="art-details-like" style={{ display: "flex", alignItems: "center" }}>{props.art.nbrLikes} <RiHeartLine /> </div>}
                    {isLiked && props.art.theme.periode.id !== 2 && <div className="art-details-like" style={{ display: "flex", alignItems: "center" }}>{props.art.nbrLikes} <RiHeartFill /> </div> }

                    <div className="art-details-tags">
                        <h4>Tags</h4>
                        {props.art.tags.map((tag, index) =>
                            <TagComponent key={index} tag={tag} />
                        )}
                    </div>
                </div>
            </div>
        </>
    )
}