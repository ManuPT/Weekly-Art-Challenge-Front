import React from 'react';
import { Art } from '../../models/Art';
import { ArtCardComponent } from './ArtCardComponent';

interface Props {
    hasBig?: boolean;
    arts: Art[];
    update: any;
}

export const ListArtComponent = (props: Props) => {
    return (
        <div className="art-list">
            {props.arts.map((art, index) =>
                <ArtCardComponent key={index} art={art} update={props.update}/>
            )}
        </div>
    )
}