import React from 'react';
import { ArtPost } from '../../models/ArtPost';

interface Props {
    art: ArtPost;
}

export const ArtContentPreviewComponent = (props: Props) => {

    const imgOrVideo = () => {
        switch(props.art.category.id) {
            case 1:
            case 5:
                return "VIDEO";
            default:
                return "IMG"  ;
        }
    }

    return (<>
        {imgOrVideo() === "IMG" ? 
            <img className="art-content" src={props.art.content} alt={props.art.name} />
        :
            <iframe className="art-content" src={props.art.content} title={props.art.name} allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
        }
        </>
    )
}