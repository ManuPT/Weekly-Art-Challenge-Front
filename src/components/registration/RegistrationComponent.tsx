import React, { useContext, useState } from 'react';
import { useHistory, useLocation } from 'react-router';
import { UserContext } from '../../App';
import { User, UserLogin, UserRegistration } from '../../models/User';
import { loginService } from '../../services/LoginHttpService';

export const RegistrationComponent = () => {

    const history = useHistory();
    
    const [username,setUsername] = useState("");
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [firstName,setFirstName] = useState("");
    const [lastName,setLastName] = useState("");
    const [description,setDescription] = useState("");

    const [usernameAuthentication,setUsernameAuthentication] = useState("");
    const [passwordAuthentication,setPasswordAuthentication] = useState("");

    const location = useLocation<any>();
    const [rightPanelActive, setRightPanelActive] = useState(location.state ? location.state.signOut : true);

    const toggleActivePanel = () => {
        setRightPanelActive(!rightPanelActive);
    }
    
    const containerClassName = rightPanelActive ? "right-panel-active" : "";

    const {user, setUser} = useContext(UserContext);

    const signUp = () => {
        const userEntry : UserRegistration = {
            pseudo: username,
            password: password,
            mail: email,
            firstname: firstName,
            lastname: lastName,
            description: description
        };

        let userReceivedRegistration : User;
        loginService.signUp(userEntry)
            .then(result => userReceivedRegistration = result)
            .then(result => localStorage.setItem("user", JSON.stringify(userReceivedRegistration)))
            .then(result => setUser(userReceivedRegistration))
            .then(result => history.push("/"));
    };

    const signIn = () => {
        
        const userAuth : UserLogin = {
            pseudo: usernameAuthentication,
            password: passwordAuthentication
        };
        const instanceOfUser = (object: any) => {
                const jsonObject = JSON.stringify(object);
                if(jsonObject != null){
                    if (jsonObject.indexOf("i") == 2){
                        return true;
                   }
                }
        };

        let userReceivedAuthentication : User; 
        loginService.signIn(userAuth)
            .then(result => instanceOfUser(result) ? userReceivedAuthentication = result : null)
            .then(result => instanceOfUser(userReceivedAuthentication) ? localStorage.setItem("user", JSON.stringify(userReceivedAuthentication)) : null)
            .then(result => instanceOfUser(userReceivedAuthentication) ? setUser(userReceivedAuthentication) : null)
            .then(result => instanceOfUser(userReceivedAuthentication) ? history.push("/") : null);
    }

    return (
        <>
            <div className={`registration-container ` + containerClassName} id="container">
                <div className="form-container sign-up-container">
                    <form onSubmit={((e) => e.preventDefault())}>
                        <h2>Create Account</h2>
                        <input type="text" placeholder="Username" value={username} onChange={(event)=>setUsername(event.target.value)}/>
                        <input type="email" placeholder="Email" value={email} onChange={(event)=>setEmail(event.target.value)}/>
                        <input type="password" placeholder="Password" value={password} onChange={(event)=>setPassword(event.target.value)}/>
                        <div className="form-group">
                        <input type="text" placeholder="First name (optionnal)" value={firstName} onChange={(event)=>setFirstName(event.target.value)}/>
                        <input type="text" placeholder="Last name (optionnal)" value={lastName} onChange={(event)=>setLastName(event.target.value)}/>
                        </div>
                        <input type="text" placeholder="Description (optionnal)" value={description} onChange={(event)=>setDescription(event.target.value)}/>
                        <button className="button-full" onClick={signUp}>Sign Up</button>
                    </form>
                </div>
                <div className="form-container sign-in-container">
                    <form onSubmit={((e) => e.preventDefault())}>
                        <h2>Sign in</h2>
                        <input type="text" placeholder="Username" value={usernameAuthentication} onChange={(event)=>setUsernameAuthentication(event.target.value)}/>
                        <input type="password" placeholder="Password" value={passwordAuthentication} onChange={(event)=>setPasswordAuthentication(event.target.value)}/>
                        <a href="#">Forgot your password?</a>
                        <button className="button-full" onClick={signIn}>Sign In</button>
                    </form>
                </div>
                <div className="overlay-container">
                    <div className="overlay">
                        <div className="overlay-panel overlay-left">
                            <h2>Welcome Back!</h2>
                            <p>To keep connected with us please login with your personal info</p>
                            <button className="button-full ghost" onClick={toggleActivePanel}>Sign In</button>
                        </div>
                        <div className="overlay-panel overlay-right">
                            <h2>Hello, Friend!</h2>
                            <p>Enter your personal details and start journey with us</p>
                            <button className="button-full ghost" onClick={toggleActivePanel}>Sign Up</button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
