export interface Theme {
  id: number;
  dateStart: Date;
  dateEnd: Date;
  name: string;
  periode: {
    id: number;
    name: string;
  };
  nbParticipation: number;
}
