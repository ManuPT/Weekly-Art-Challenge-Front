export interface Follow {
  user: {id: number};
  userFollowed: {id: number};
}