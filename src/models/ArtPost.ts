import { Tag } from "./Tag";

export interface ArtPost {
  content: string;
  name: string;
  category: {
    id: number;
  };
  user: {
    id: number;
  };
  tags: Tag[];
}