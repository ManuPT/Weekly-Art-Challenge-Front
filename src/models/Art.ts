import { Tag } from "./Tag";

export interface Art {
  id: number;
  content: string;
  name: string;
  category: {
    id: number;
    name: string;
  };
  theme: {
    id: number;
    name: string;
    periode: {
      id: number,
    }
  };
  user: {
    id: number;
    pseudo: string;
  };
  tags: Tag[];
  nbrLikes: number;
}
