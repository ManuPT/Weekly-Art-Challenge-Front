export interface SocialMedia {
  id: number;
  link: string;
  logo: {
    id: number;
    logo: string;
    name: string;
  };
}
