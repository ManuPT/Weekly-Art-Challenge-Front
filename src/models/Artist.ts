import { Art } from "./Art";
import { SocialMedia } from "./SocialMedia";
import { Tag } from "./Tag";

export interface Artist {
  id: number;
  description: string;
  firstname: string;
  lastname: string;
  mail: string;
  pseudo: string;
  tags: Tag[];
  arts: Art[];
  socialMedias: SocialMedia[];
  soumission: Art;
  nbParticipation: number;
  nbFollow: number;
}