export interface User {
  id: number;
  description: string;
  firstname: string;
  lastname: string;
  mail: string;
  pseudo: string;
  role: {
    id: number;
    name: string;
  };
}

export interface UserLogin {
    password: string;
    pseudo: string;
}

export interface UserRegistration {
    pseudo: string;
    password: string;
    mail: string;
    firstname: string;
    lastname: string;
    description: string;
}