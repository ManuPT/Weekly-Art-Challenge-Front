import React, { createContext, useEffect, useState } from 'react'
import './App.css';
import { Header } from './components/header/Header';
import { Routing } from './Routing';
import { BrowserRouter } from 'react-router-dom';
import { User } from './models/User';
import { IconContext } from "react-icons";

function App() {

  const userEmpty : User = {
    id:-1,
    pseudo:"",
    mail:"",
    role:{
        id:-1,
        name:""
    },
    firstname:"",
    lastname:"",
    description:""
  }

  /**
  * Retourne le User du LocalStorage s'il existe sinon un User dont l'id est "-1"
  */
  const localUser = () =>{
  if (localStorage.getItem("user"))
       return JSON.parse(localStorage.getItem("user") || "");
  else
      return userEmpty;
  }
  const [user,setUser] = useState(localUser());
  const value = {user, setUser};

  const changeUser = () => {
      setUser(localStorage.getItem("user"))
    }



  return (
    <div className="App">
      <BrowserRouter>
        <UserContext.Provider value={value}>
        <IconContext.Provider value={{ style: { verticalAlign: 'middle' } }}>
            <Header />
              <div className="main-container">
                <Routing />
              </div>
        </IconContext.Provider>
        </UserContext.Provider>
      </BrowserRouter>
    </div>
  );
}

export const UserContext = createContext(
  {user: {
    id:-1,
    pseudo:"",
    mail:"",
    role:{
        id:-1,
        name:""
    },
    firstname:"",
    lastname:"",
    description:""
  }, 
  setUser : (user : User) => {}});
export default App;
